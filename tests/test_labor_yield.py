# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest

import doctest

from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.tests.test_tryton import suite as test_suite
from trytond.tests.test_tryton import doctest_teardown, doctest_checker
from trytond.pool import Pool
from datetime import date
from trytond.modules.company.tests import create_company, set_company
from trytond.exceptions import UserError


class LaborYieldTestCase(ModuleTestCase):
    """Test Labor Yield module"""
    module = 'labor_yield'

    @staticmethod
    def create_company_employee_work():
        pool = Pool()
        Party = pool.get('party.party')
        Employee = pool.get('company.employee')
        Work = pool.get('timesheet.work')
        company = create_company()
        with set_company(company):
            party = Party(name='Pam Beesly')
            party.save()
            employee = Employee(party=party.id, company=company)
            employee.save()
            work = Work(name='Yield work', yield_available=True)
            work.save()
        return company, party, employee, work

    @with_transaction()
    def test_manual_allocation_recording(self):
        """Test manual allocation recording"""

        pool = Pool()
        company, party, employee, work = self.create_company_employee_work()
        work.manual_yield_record = True
        work.save()
        YAProcedure = pool.get('labor.yield.allocation.procedure')
        proc = YAProcedure(name='Test LY Procedure', allowed_offsets=3,
            work=work)
        proc.save()
        YAlloc = pool.get('labor.yield.allocation')
        y_alloc = YAlloc(date=date.today(), employee=employee, quantity=1000,
            procedure=proc, work=work, quantity_uom=work.uom_category.uoms[0])
        self.assertRaises(UserError, y_alloc.save)
        y_alloc.procedure = None
        y_alloc.save()

    @with_transaction()
    def test_invalid_offset_spec_error(self):
        """Test invalid offset specification error"""

        pool = Pool()
        company, party, employee, work = self.create_company_employee_work()
        YAProcedure = pool.get('labor.yield.allocation.procedure')
        proc = YAProcedure(name='Test LY Procedure', allowed_offsets=3,
            work=work)
        proc.save()
        YAlloc = pool.get('labor.yield.allocation')
        y_alloc = YAlloc(date=date.today(), employee=employee, quantity=1000,
            procedure=proc, work=work, quantity_uom=work.uom_category.uoms[0])
        self.assertRaises(UserError, y_alloc.save)
        y_alloc.offset_ = 1
        y_alloc.save()

    @with_transaction()
    def test_company_granularity_allocation_recording(self):
        """Test company granularity allocation recording"""

        pool = Pool()
        company, _, _, work = self.create_company_employee_work()
        work.manual_yield_record = True
        work.save()
        YAlloc = pool.get('labor.yield.allocation')
        y_alloc = YAlloc(date=date.today(), quantity=1000, work=work,
            quantity_uom=work.uom_category.uoms[0])
        self.assertRaises(UserError, y_alloc.save)
        work.yield_record_granularity = 'company'
        work.save()
        y_alloc.save()


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            LaborYieldTestCase))
    suite.addTests(doctest.DocFileSuite(
        'scenario_labor_yield.rst',
        tearDown=doctest_teardown, encoding='utf-8',
        checker=doctest_checker,
        optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    return suite
