# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields, DeactivableMixin
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Bool, Not, If, And, Equal
from trytond.transaction import Transaction


class YieldAllocationMixin(object):
    """Base for Yield Allocations"""

    work = fields.Many2One('timesheet.work', 'Work', required=True,
        select=True, ondelete='RESTRICT',
        domain=[('yield_available', '=', True)])
    manual_yield_record = fields.Function(
        fields.Boolean('Allow manual yield record'),
        'get_manual_yield_record')
    employee = fields.Many2One('company.employee', 'Employee', select=True,
        ondelete='RESTRICT',
        states={'readonly': Not(Eval('manual_yield_record'))},
        depends=['manual_yield_record'])
    quantity = fields.Float('Quantity', required=True,
        digits=(16, Eval('quantity_digits', 2)),
        states={'readonly': Not(Eval('manual_yield_record'))},
        depends=['quantity_digits', 'manual_yield_record'])
    quantity_uom = fields.Many2One('product.uom', 'Quantity UoM',
        required=True,
        domain=[
            ('category', '=',
                Eval('_parent_work', {}).get('uom_category'))],
        states={'readonly': Not(Eval('manual_yield_record'))},
        depends=['manual_yield_record'])
    quantity_digits = fields.Function(
        fields.Integer('Quantity Digits'),
        'on_change_with_quantity_digits')
    procedure = fields.Many2One('labor.yield.allocation.procedure',
        'Procedure', readonly=True, select=True, ondelete='RESTRICT',
        states={'required': Not(Eval('manual_yield_record'))},
        depends=['manual_yield_record'])

    @classmethod
    def default_work(cls):
        return Transaction().context.get('work', None)

    @fields.depends('work', 'quantity_uom')
    def on_change_work(self):
        if self.work:
            if (not self.quantity_uom
                    or self.quantity_uom.category
                    != self.work.default_uom.category):
                self.quantity_uom = self.work.default_uom
                self.manual_yield_record = self.work.manual_yield_record
        else:
            self.quantity_uom = None

    @staticmethod
    def default_manual_yield_record():
        return True

    def get_manual_yield_record(self, name):
        return self.work.manual_yield_record

    @classmethod
    def default_quantity_uom(cls):
        pool = Pool()
        Work = pool.get('timesheet.work')
        work = cls.default_work()
        work = Work(work) if work else None
        if work:
            return work.default_uom.id

    @fields.depends('quantity_uom')
    def on_change_with_quantity_digits(self, name=None):
        if self.quantity_uom:
            return self.quantity_uom.digits
        else:
            self.default_quantity_digits()

    @staticmethod
    def default_quantity_digits():
        return 0


class YieldAllocation(YieldAllocationMixin, ModelSQL, ModelView):
    """Labor Yield Allocation"""
    __name__ = 'labor.yield.allocation'
    date = fields.Date('Date', required=True, select=True,
        states={'readonly': Not(Eval('manual_yield_record'))},
        depends=['manual_yield_record'])
    source = fields.Reference('Yield source', selection='get_source_name',
        select=True, readonly=True,
        states={
            'required':
                And(Bool(Eval('procedure', False)),
                    Equal(Eval('offset_', 0), 0))},
        depends=['offset_', 'procedure'])
    allowed_offsets = fields.Function(
        fields.Integer('Allowed offsets'),
        'get_allowed_offsets')
    offset_ = fields.Integer('Offset', readonly=True,
        domain=[
            If(Bool(Eval('offset_', None)),
                ('offset_', '<=', Eval('allowed_offsets')), ())],
        states={
            'required': And(
                Bool(Eval('procedure', False)),
                Not(Bool(Eval('source', False))))},
        depends=['procedure', 'source', 'allowed_offsets'])

    @classmethod
    def __setup__(cls):
        super(YieldAllocation, cls).__setup__()

        rq_st = Equal(Eval('_parent_work',
            {}).get('yield_record_granularity', 'employee'), 'employee')

        cls.employee.states = {
            'required': rq_st,
            'readonly': Not(rq_st),
            'invisible': Not(rq_st)}
        cls.employee.depends = ['work']

    @staticmethod
    def default_allowed_offsets():
        return 0

    def get_allowed_offsets(self, name=None):
        return self.procedure and self.procedure.allowed_offsets or 0

    @fields.depends('procedure')
    def on_change_procedure(self):
        self.allowed_offsets = self.get_allowed_offsets()

    @classmethod
    def _get_source_name(cls):
        return ['']

    @classmethod
    def get_source_name(cls):
        Model = Pool().get('ir.model')
        models = cls._get_source_name()
        models = Model.search([
            ('model', 'in', models),
        ])
        return [('', '')] + [(m.model, m.name) for m in models]

    @classmethod
    def validate(cls, records):
        super(YieldAllocation, cls).validate(records)
        offset_inv = [r for r in records
            if r.procedure and bool(r.source) == bool(r.offset_)]
        if offset_inv:
            raise UserError(gettext(
                'labor_yield.msg_labor_yield_allocation_invalid_offset_spec',
                offset=offset_inv))

    @staticmethod
    def default_date():
        Date_ = Pool().get('ir.date')
        return Transaction().context.get('date') or Date_.today()

    @classmethod
    def view_attributes(cls):
        attrs = super(YieldAllocation, cls).view_attributes()
        pool = Pool()
        work_id = Transaction().context.get('work', None)
        if work_id:
            Work = pool.get('timesheet.work')
            work = Work(work_id)
            if (work.yield_available
                    and work.yield_record_granularity == 'company'):
                attrs += [
                    ('/tree//field[@name="employee"]', 'tree_invisible', 1)]

        return attrs


class YieldAllocationProcedure(DeactivableMixin, ModelSQL, ModelView):
    """Labor Yield Allocation Procedure"""
    __name__ = 'labor.yield.allocation.procedure'

    name = fields.Char('Name', required=True, select=True, translate=True)
    work = fields.Many2One('timesheet.work', 'Work',
        select=True, ondelete='RESTRICT',
        domain=[('yield_available', '=', True)])
    allowed_offsets = fields.Integer('Allowed offsets',
        domain=[('allowed_offsets', '>', 0)])

    @staticmethod
    def default_allowed_offsets():
        return 1

    def get_allocations(self, date):
        return []

    def get_unallocated(self, date):
        return []


class YieldAllocation2(metaclass=PoolMeta):
    __name__ = 'labor.yield.allocation'

    work_center = fields.Function(
            fields.Many2One('production.work.center', 'Work center'),
        'get_work_center')

    def get_work_center(self, name):
        with Transaction().set_context(date=self.date):
            if self.employee.work_center:
                return self.employee.work_center.id
        return None

    @classmethod
    def view_attributes(cls):
        attrs = super(YieldAllocation, cls).view_attributes()
        if ('/tree//field[@name="employee"]', 'tree_invisible', 1) in attrs:
            attrs += [
                ('/tree//field[@name="work_center"]', 'tree_invisible', 1)]
        return attrs


class YieldAllocation3(metaclass=PoolMeta):
    __name__ = 'labor.yield.allocation'

    employee_code = fields.Function(fields.Char('Employee Code'),
        'on_change_with_employee_code')

    @fields.depends('employee')
    def on_change_with_employee_code(self, name=None):
        return self.employee.code if self.employee else None
