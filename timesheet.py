# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Not, Bool


class Work(metaclass=PoolMeta):
    __name__ = 'timesheet.work'

    _RQ_ST = Bool(Eval('yield_available'))

    yield_available = fields.Boolean('Available on yield')
    yield_record_granularity = fields.Selection([
        ('company', 'Company'),
        ('employee', 'Employee')], 'Yield record granularity',
        states={'readonly': Not(_RQ_ST)},
        depends=['yield_available'])
    manual_yield_record = fields.Boolean('Allow manual yield record',
        states={'readonly': Not(_RQ_ST)},
        depends=['yield_available'])
    default_uom = fields.Many2One('product.uom', 'Default uom',
        ondelete='RESTRICT',
        states={
            'required': _RQ_ST,
            'readonly': Not(_RQ_ST)},
        depends=['yield_available'])
    uom_category = fields.Function(
        fields.Many2One('product.uom.category', 'Uom category'),
        'get_uom_category')

    @classmethod
    def default_yield_record_granularity(cls):
        return 'employee'

    @classmethod
    def default_manual_yield_record(cls):
        return False

    @classmethod
    def default_yield_available(cls):
        return False

    @staticmethod
    def default_uom_category():
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        return ModelData.get_id('product', 'uom_cat_unit')

    def get_uom_category(self, name):
        if self.default_uom:
            return self.default_uom.category.id

    @staticmethod
    def default_default_uom():
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        return ModelData.get_id('product', 'uom_unit')

    @fields.depends('default_uom')
    def on_change_default_uom(self):
        if self.default_uom:
            self.uom_category = self.default_uom.category
        else:
            self.uom_category = None

    @classmethod
    def view_attributes(cls):
        return super(Work, cls).view_attributes() + [
            ('/form/notebook/page[@id="yield"]', 'states', {
                'invisible': ~Eval('yield_available')})]
