# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import timesheet
from . import labor_yield
from . import yield_wizards


def register():
    Pool.register(
        timesheet.Work,
        labor_yield.YieldAllocationProcedure,
        labor_yield.YieldAllocation,
        yield_wizards.YieldAllocateProcedure,
        yield_wizards.YieldAllocateGetParams,
        yield_wizards.YieldAllocateSummary,
        yield_wizards.YieldAllocateSummaryDet,
        yield_wizards.YieldEnterGetParams,
        module='labor_yield', type_='model')
    Pool.register(
        yield_wizards.YieldEnter,
        yield_wizards.YieldAllocate,
        module='labor_yield', type_='wizard')
    Pool.register(
        labor_yield.YieldAllocation2,
        module='labor_yield', type_='model',
        depends=['production_work_employee'])
    Pool.register(
        labor_yield.YieldAllocation3,
        yield_wizards.YieldAllocateSummaryDet3,
        module='labor_yield', type_='model',
        depends=['company_employee_code'])
